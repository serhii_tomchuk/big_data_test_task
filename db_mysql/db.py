class MySqlDb:
    def __init__(self, dsn, sql):
        self.dsn = dsn
        self.sql = sql

    def _connect(self):
        import mysql.connector
        return mysql.connector.connect(**self.dsn)

    def create_table(self):
        conn = self._connect()
        cursor = conn.cursor()
        cursor.execute(self.sql["ORDERS_TABLE"])
        return conn, cursor

    def filling_table_from_csv(self, orders):
        conn = self._connect()
        cursor = conn.cursor()
        cursor.executemany(self.sql["CREATE_ORDERS"], orders)
        conn.commit()

    def drop_table(self):
        conn = self._connect()
        cursor = conn.cursor()
        cursor.execute(self.sql["DROP_ORDERS"])
        return conn, cursor

    def count_rows(self):
        conn = self._connect()
        cursor = conn.cursor()
        cursor.execute(self.sql["COUNT_ROWS_IN_DB"])
        return cursor.fetchall()

    def data_for_a_month(self):
        conn = self._connect()
        cursor = conn.cursor()
        cursor.execute(self.sql["SELECT_NUMERICAL_ANALYSIS"])
        return cursor.fetchall()
