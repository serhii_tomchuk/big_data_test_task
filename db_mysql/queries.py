QUERIES = dict(
    ORDERS_TABLE='''
            CREATE TABLE IF NOT EXISTS ORDERS (
                    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
                    observation   VARCHAR (255),
                    market        VARCHAR (255),
                    conversion_id VARCHAR (255),
                    hashed_email  VARCHAR (255)
                )''',

    CREATE_ORDERS='''
            INSERT INTO ORDERS
            (observation, market, conversion_id, hashed_email)
            VALUES (%s, %s, %s, %s)
    ''',

    DROP_ORDERS="""DROP TABLE ORDERS""",

    COUNT_ROWS_IN_DB="""SELECT COUNT(hashed_email) FROM ORDERS""",

    SELECT_NUMERICAL_ANALYSIS="""
    SELECT v1.market AS Market,
        v1.Order_Month AS Order_Month,
    COUNT(*) as Total_orders,
    COUNT(CASE WHEN v1.minOrderMonth = v1.Order_Month THEN 1 ELSE NULL END)
    AS N_New_Orders,
    COUNT(CASE WHEN v1.minOrderMonth = v1.Order_Month THEN NULL ELSE 1 END)
    AS N_Return_Orders,
        '100%' AS "Orders"
    FROM(
      SELECT  t1.*,
          date_format(t1.observation, '%Y-%m-01') AS Order_Month,
          v2.minObservation,
          date_format(v2.minObservation, '%Y-%m-01') as minOrderMonth
      FROM   ORDERS t1
      LEFT JOIN   (
              SELECT hashed_email,
                  MIN(observation) as minObservation
              FROM ORDERS
              GROUP BY hashed_email
            ) v2 ON v2.hashed_email = t1.hashed_email
    ) v1
WHERE   v1.market IN ("NL","DE")
GROUP BY v1.market,
    v1.Order_Month
;
    """
)
