from itertools import count

from db_mysql.db import MySqlDb
from db_mysql.queries import QUERIES
from db_mysql.settings import mysql_config
from helper.analysis import Analysis
from helper.intermadiate_storage import StorageDict
from helper.order_report import OrderReport

mysql_ = MySqlDb(dsn=mysql_config, sql=QUERIES)
storage_ = StorageDict()
order_data = OrderReport()

if __name__ == "__main__":
    # mysql_.drop_table()
    # exit(0)

    mysql_.create_table()
    orders = order_data.save_order_data()

    if mysql_.count_rows()[0][0] != len(orders):
        mysql_.filling_table_from_csv(orders=orders)

    numerical_analysis_data = mysql_.data_for_a_month()
    c = count(1)
    [
        storage_.add(
            next(c),
            dict(
                Market=item[0],
                Order_Month=item[1],
                Total_orders=item[2],
                N_New_Orders=item[3],
                N_Return_Orders=item[4],
                Sum_Return_Orders=item[5]
            )
        )
        for item in numerical_analysis_data
    ]
    writer_analysis = Analysis(data_to_write=storage_.__dict__["_dict"])
    writer_analysis.write_csv()
