class StorageDict(dict):

    def __init__(self):
        super().__init__()
        self._dict = {}

    def add(self, key_, val):
        self._dict[key_] = val
