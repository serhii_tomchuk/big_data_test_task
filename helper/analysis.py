import csv


class Analysis:
    FILENAME = "order_analysis.csv"

    def __init__(self, data_to_write):
        self.data_to_write = data_to_write

    def write_csv(self):
        field_names = self.data_to_write.get(1).keys()
        all_dicts = self.data_to_write

        with open(self.FILENAME, "w", newline="", encoding="utf-8") as file:
            writer = csv.DictWriter(file, fieldnames=field_names)
            writer.writeheader()

            for row in all_dicts:
                writer.writerow(all_dicts[row])
