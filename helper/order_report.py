import csv
import os


class OrderReport:

    @staticmethod
    def _find_csv():
        all_files = os.listdir("./")
        return list(filter(lambda f: f.endswith('.csv'), all_files))

    def _get_order_data(self):
        try:
            with open(self._find_csv()[0], newline='') as csv_file:
                orders = csv.reader(csv_file, delimiter=' ', quotechar='|')
                return list(orders)[1::]
        except FileNotFoundError as err:
            print(err)

    def save_order_data(self):
        return list(map(
            lambda i: tuple(i[0].split(',')),
            self._get_order_data()
        ))
